/*
 * Copyright 2019-2024 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */

public class Hello5
{
    public void foo()
    {
        if (isHashCodeEven()) {
            System.out.println("foo if branch");
            System.out.println("foo if branch");
            System.out.println("foo if branch");
            System.out.println("foo if branch");
            System.out.println("foo if branch");
        }
        else {
            System.out.println("foo else branch");
            System.out.println("foo else branch");
            System.out.println("foo else branch");
            System.out.println("foo else branch");
            System.out.println("foo else branch");
        }
    }



    public void bar()
    {
        if (isHashCodeEven()) {
            System.out.println("bar if branch");
            System.out.println("bar if branch");
            System.out.println("bar if branch");
            System.out.println("bar if branch");
            System.out.println("bar if branch");
        }
        else {
            System.out.println("bar else branch");
            System.out.println("bar else branch");
            System.out.println("bar else branch");
            System.out.println("bar else branch");
            System.out.println("bar else branch");
        }
    }



    private boolean isHashCodeEven()
    {
        return getClass().getSimpleName().hashCode() % 2 == 0;
    }
}
