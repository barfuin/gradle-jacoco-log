/*
 * Copyright 2019-2024 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.xml.parsers.ParserConfigurationException;

import groovy.util.Node;
import org.gradle.api.GradleException;
import org.xml.sax.SAXException;


/**
 * Wrapper around Groovy's XML parser. Depending on the Groovy version, the class name may differ.
 * <ul>
 *     <li>Groovy &lt;=2, Gradle &lt;=6 → <code>groovy.util.XmlParser</code></li>
 *     <li>Groovy &gt;=3, Gradle &gt;=7 → <code>groovy.xml.XmlParser</code></li>
 * </ul>
 * <p>We use reflection to find the right class and invoke it.</p>
 */
public class ParserProxy
{
    private final Object parser;



    @SuppressWarnings("ConstantConditions")
    public ParserProxy()
        throws ParserConfigurationException, SAXException
    {
        Class<?> parserClass = null;
        try {
            parserClass = Class.forName("groovy.xml.XmlParser");
        }
        catch (ClassNotFoundException e) {
            try {
                parserClass = Class.forName("groovy.util.XmlParser");
            }
            catch (ClassNotFoundException e2) {
                throw new GradleException("Internal error: XML parser not found", e2);
            }
        }

        Object newParser = null;
        try {
            Constructor<?> constructor = parserClass.getConstructor(boolean.class, boolean.class, boolean.class);
            newParser = constructor.newInstance(false, true, true);
        }
        catch (Exception e) {
            if (e instanceof ParserConfigurationException) {
                throw (ParserConfigurationException) e;
            }
            if (e instanceof SAXException) {
                throw (SAXException) e;
            }
            if (e instanceof ReflectiveOperationException || e instanceof RuntimeException) {
                throw new GradleException("Internal error: Failed to instantiate XML parser", e);
            }
        }

        if (newParser == null) {
            throw new GradleException("Internal error: Failed to instantiate XML parser");
        }
        parser = newParser;

        turnOffDtdChecking();
    }



    private void turnOffDtdChecking()
    {
        try {
            Method method = parser.getClass().getMethod("setFeature", String.class, boolean.class);
            method.invoke(parser, "http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        }
        catch (ReflectiveOperationException | RuntimeException e) {
            throw new GradleException("Internal error: Failed to configure XML parser", e);
        }
    }



    public Node parse(final File pInputFile)
        throws IOException, SAXException
    {
        try {
            Method method = parser.getClass().getMethod("parse", File.class);
            return (Node) method.invoke(parser, pInputFile);
        }
        catch (InvocationTargetException e) {
            if (e.getCause() instanceof IOException) {
                throw (IOException) e.getCause();
            }
            if (e.getCause() instanceof SAXException) {
                throw (SAXException) e.getCause();
            }
            throw new GradleException("Internal error: Failed to invoke XML parser", e);
        }
        catch (NoSuchMethodException | IllegalAccessException e) {
            throw new GradleException("Internal error: Failed to invoke XML parser", e);
        }
    }
}
