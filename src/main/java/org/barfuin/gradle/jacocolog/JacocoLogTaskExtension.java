/*
 * Copyright 2019-2024 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

/**
 * Task extension applied to {@link org.gradle.api.tasks.testing.Test Test} tasks which adds properties required by
 * our plugin.
 */
public class JacocoLogTaskExtension
{
    public static final String TASK_EXTENSION_NAME = "jacocoLog";

    // TODO This may not work at all, because we can't really add it to subprojects. The subproject might evaluate
    //      the task config before we add the task extension.
}
