/*
 * Copyright 2019-2024 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.NotThreadSafe;


/**
 * A list which is able to execute actions on all its elements, including elements added later than the action.
 * This functionality is identical to that provided by Gradle's {@link org.gradle.api.NamedDomainObjectContainer
 * NamedDomainObjectContainer}, and was added here to work around some problems where actions were in fact <em>not</em>
 * executed.
 *
 * @param <T> the type of the elements of the list
 */
@NotThreadSafe
public class ActionableList<T>
{
    private final List<T> elements = new ArrayList<>();

    private final List<Consumer<T>> actions = new ArrayList<>();



    public void all(@Nonnull final Consumer<T> pAction)
    {
        actions.add(pAction);
        elements.forEach(this::executeActionsOn);
    }



    public void add(@Nullable final T pElement)
    {
        if (pElement != null) {
            elements.add(pElement);
            executeActionsOn(pElement);
        }
    }



    private void executeActionsOn(@Nonnull final T pElement)
    {
        actions.forEach(action -> action.accept(pElement));
    }
}
