/*
 * Copyright 2019-2024 The gradle-jacoco-log contributors
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */
package org.barfuin.gradle.jacocolog;

import java.util.Objects;

import groovy.lang.Closure;
import org.gradle.api.Action;
import org.gradle.api.Task;


/**
 * Wraps an action into a closure.
 * <p>This is used for passing lambdas to <code>doFirst()</code>/<code>doLast()</code> calls without compromising
 * Gradle's up-to-date checking:
 * <a href="https://docs.gradle.org/8.6/userguide/incremental_build.html#sec:how_does_it_work"
 * >https://docs.gradle.org/8.6/userguide/incremental_build.html#sec:how_does_it_work</a></p>
 */
public class ActionClosure<T extends Task>
    extends Closure<Void>
{
    private final Action<T> action;



    public ActionClosure(final Task pOwningTask, final Action<T> pAction)
    {
        super(pOwningTask);
        this.action = Objects.requireNonNull(pAction, "Argument pAction was null");
    }



    @SuppressWarnings({"unchecked", "MethodDoesntCallSuperMethod"})
    @Override
    public Void call()
    {
        action.execute((T) getDelegate());
        return null;
    }
}
