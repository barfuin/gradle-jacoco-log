# gradle-jacoco-log

Gradle plugin that logs test coverage calculated by the
[JaCoCo Gradle Plugin](https://docs.gradle.org/current/userguide/jacoco_plugin.html) to the Gradle build log.
This plugin can also aggregate coverage information from multiple modules in a multi-module build.


## How to use

*gradle-jacoco-log* is published to the
[Gradle Plugin Portal](https://plugins.gradle.org/plugin/org.barfuin.gradle.jacocolog),
so you can just use it in your build like so:

**build.gradle:**

```groovy
plugins {
    id 'org.barfuin.gradle.jacocolog' version '3.1.0'
}
```

That's it! Every time a
[JacocoReport](https://docs.gradle.org/8.6/dsl/org.gradle.testing.jacoco.tasks.JacocoReport.html) task runs
(e.g. `jacocoTestReport`), the test coverage will be logged like this:

```
Test Coverage:
    - Class Coverage: 100%
    - Method Coverage: 100%
    - Branch Coverage: 81.2%
    - Line Coverage: 97.8%
    - Instruction Coverage: 95.3%
    - Complexity Coverage: 90.2%
```

In order to report aggregated coverage of all the subprojects, just call:

```
gradle test jacocoAggregatedReport
```

As shown in the example above, it is important that all test tasks whose results shall be aggregated are also invoked
before `jacocoAggregatedReport`, so that they actually run and can be aggregated.


#### Prerequisites

- Gradle 5 or newer
- Java 8 or newer


&nbsp;

----------------------------------------------------------------------------------------------------------------------

## Details

Next are some more details in case you have a complex build, or want to do advanced stuff.

### Where to apply in multi-module projects

For test coverage calculation and logging, several plugins are working together:

- the `java` plugin (from Gradle, provide *test* task and others)
- the `jacoco` plugin (from Gradle, provides *jacocoTestReport* task and others)
- the `org.barfuin.gradle.jacocolog` plugin (us, provides *jacocoLogTestCoverage* task and others)

You add `jacoco` to any module where you want to have test coverage *measured*.  
You add `org.barfuin.gradle.jacocolog` to any module where you want to have test coverage *logged*.  
This applies also for aggregated coverage. If you are only interested in aggregated coverage, you only need to add
`org.barfuin.gradle.jacocolog` to the parent project (whereever the aggregation should happen). It will then aggregate
the coverage of all subprojects which have the `jacoco` plugin (where stuff was *measured*).

Two examples:

- You want to just log coverage whereever it is measured.  
  ⇒ Then you might just use `org.barfuin.gradle.jacocolog` *instead* of the `jacoco` plugin everywhere.
  (`org.barfuin.gradle.jacocolog` automatically applies `jacoco`.)
- You only want to log aggregated coverage for all the subprojects.  
  ⇒ Then add `org.barfuin.gradle.jacocolog` only to the root project, and add `jacoco` to all subprojects that shall
  be included in the aggregation.


### Tasks

This plugin provides the following tasks:

- <b>jacocoLog<i>Xxx</i>Coverage</b> (type: LogCoverageTask) –
  For each JacocoReport task in the current project, an instance of this task is added by this plugin. It logs the
  coverage information to the Gradle build log. The naming of these "log tasks" follows the naming convention explained
  below. Each log task has a dependency on its corresponding JacocoReport task, and the JacocoReport task is finalized
  by the log task.
- **jacocoAggregatedReport** (type:
  [JacocoReport](https://docs.gradle.org/8.6/dsl/org.gradle.testing.jacoco.tasks.JacocoReport.html)) –
  Produces a JacocoReport on the aggregated data from all test tasks and JacocoReport tasks in the current project and
  all subprojects. The log task created for this task is called *jacocoLogAggregatedCoverage*. This task can handle it
  if your JacocoReport tasks have been reconfigured, for example to exclude certain classes. It should also deal with
  most other special cases, such as missing tests, or custom JacocoReport tasks.


### Task Naming Convention

The Gradle Jacoco Plugin implies a
[naming convention](https://github.com/gradle/gradle/blob/v8.6.0/platforms/jvm/jacoco/src/main/java/org/gradle/testing/jacoco/plugins/JacocoPlugin.java#L225)
for its report tasks, which is "jacoco*TestTask*Report". In this way, the relationship between tasks, and the task
names are indicated. Two examples:

| Test task name<br>(from java plugin)| Report task name<br>(from Jacoco plugin) | Log task name<br>(from this plugin) |
|:------------------------------------|:-----------------------------------------|:------------------------------------|
| test                                | jacocoTestReport                         | jacocoLogTestCoverage               |
| foobar                              | jacocoFoobarReport                       | jacocoLogFoobarCoverage             |

So, if you define your own JacocoReport tasks in your build, you should follow this naming convention. If you don't,
we'll improvise by calling our log tasks `jacocoLogCoverage1` and so on, with a number postfix.

All this naming stuff only concerns you if you have custom JacocoReport tasks in your build.


### Disabling the logging

In order to disable coverage logging for one particular JacocoReport task, you can simply disable the corresponding
log task (log task name according to naming convention above):

```groovy
jacocoLogFoobarCoverage {   // <-- this is the name of the log task
    enabled = false
}
```


### Configuration

There is a small number of configuration options.

#### Which Counters to Log

It can be configured which counters are shown in the log. By default, all counters are shown, but you can hide
individual counters like so:

```groovy
jacocoLogTestCoverage {
    counters {
        showComplexityCoverage = false
        showClassCoverage = false
        showLineCoverage = false
    }
}
```

There are also convenience methods for all counters such as `showLineCoverageOnly()`, which set one flag and disable
all others. They can be combined.

#### Log when the Jacoco Report is Up-to-date, or not

In addition to that, it can be controlled if coverage should be logged even when the corresponding JacocoReport task
is up-to-date:

```groovy
jacocoLogTestCoverage {
    logAlways = false
    counters {
        // ...
    }
}
```

When `logAlways` is `true` (the default), we always log the coverage information. This is good for CI, because a tool
like GitLab CI will always see a value and thus it will always know the current coverage, even if it's the same as
before.  
When `logAlways` is `false`, we log coverage information only if the corresponding JacocoReport task was executed, so
we actually have something new to report. This is good for CLI use on a developer machine, because the developer won't
get spammed with coverage information.  
You might also set something like `logAlways = Boolean.parseBoolean (System.getenv("CI"))`, which will set the flag
to `true` in a CI environment, and to `false` on the local machine. The environment variable to check depends on your
CI tool, but `CI` is pretty common.


#### Number of decimal digits

By default, we log at most one decimal digit (e.g. `42.1%`). Change this with the following option:

```groovy
jacocoLogTestCoverage {
    maxDecimalDigits = 1
}
```

We don't log trailing zeros, so you'll see `100%` instead of `100.0%`.


### Integration with GitLab CI

GitLab CI can [parse the coverage
information](https://docs.gitlab.com/15.3/ee/ci/pipelines/settings.html#merge-request-test-coverage-results) from the log file.
It can only deal with a single value, so you must choose one of the counters. Line or instruction coverage are popular
for that.

In your pipeline definition, add a `coverage` entry, with `Instruction` replaced with whatever your counter is
([example](https://gitlab.com/barfuin/gradle-jacoco-log/blob/v1.2.4/.gitlab-ci.yml#L35)).

**.gitlab-ci.yml:**
```yaml
coverage: '/    - Instruction Coverage: ([0-9.]+)%/'
```

&nbsp;

----------------------------------------------------------------------------------------------------------------------

## Development

In order to develop *gradle-jacoco-log* and build it on your local machine, you need:

- Java 8 JDK ([download](https://adoptopenjdk.net/releases.html?variant=openjdk8))
- Gradle, but we use the Gradle Wrapper, so there is nothing to install for you

The project is IDE agnostic. Just use your favorite IDE and make sure none of its control files get checked in.


## Status

*gradle-jacoco-log* is feature complete and stable. It is ready for enterprise deployment.


## License

*gradle-jacoco-log* is free software under the terms of the Apache License, Version 2.0.
Details in the [LICENSE](LICENSE) file.


## Contributing

Contributions of all kinds are very welcome! If you are going to spend a lot of time on your contribution, it may
make sense to raise an issue first and discuss your contribution. Thank you!
