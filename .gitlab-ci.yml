# Copyright 2019-2024 The gradle-jacoco-log contributors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
# the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
# an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations under the License.

stages:
    - build
    - release

variables:
    LC_ALL: 'C.UTF-8'

workflow:
    rules:
        - if: $CI_MERGE_REQUEST_IID
        - if: $CI_COMMIT_TAG
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
    image: 'gradle:8.6-jdk8'
    before_script:
        - 'gradle --version > /dev/null'
        - 'java -version'


build:
    stage: build
    script:
        - 'gradle --stacktrace --no-daemon clean build'
    coverage: '/    - Line Coverage: ([0-9.]+)%/'
    rules:
        - when: on_success
    artifacts:
        expire_in: 1 day
        paths:
            - '.gradle/'
            - 'build/'
        reports:
            junit:
                - build/test-results/test/TEST-*.xml
                - build/test-results/functionalTest/TEST-*.xml


release:
    stage: release
    needs: ['build']
    script:
        - 'echo "Publishing ${CI_PROJECT_NAME} ${CI_COMMIT_TAG}"'
        - >
          gradle --no-daemon --stacktrace
          -Pgradle.publish.key=${GRADLE_PUBLISH_KEY}
          -Pgradle.publish.secret=${GRADLE_PUBLISH_SECRET}
          publishPlugins
    rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG =~ /^v[0-9][0-9.]+(?:-[0-9A-Za-z\.-]+)?$/'
